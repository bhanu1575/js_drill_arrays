
function each ( elements, callBackFunction ) {
    if(Array.isArray(elements)) {
        for( let index = 0; index < elements.length; index++) {
            if(elements[index]!==undefined) {
                callBackFunction(elements[index],index,elements);
            }
        }
    } else {
        throw new Error('Agument passed to the function each is not of type Array')
    }
}


module.exports = each;

