
function filter( elements, callbackFunction ) {
    if(Array.isArray(elements)) {
        let filteringArray = [];
        for( let index = 0; index < elements.length; index++ ) {
            if(callbackFunction(elements[index],index,elements)) {
                filteringArray.push(elements[index]);
            }
        }
        return filteringArray;
    } else {
        throw new Error('Argument passed to the function find is not of Array type');
    }
}

module.exports = filter;