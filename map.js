

function map ( elements, callbackFunction ) {
    if(Array.isArray(elements)) {
        let mapping_array = [];
        for(let index = 0; index < elements.length; index++) {
            if(elements[index] !== undefined) {
                let newElement = callbackFunction(elements[index]);
                mapping_array.push(newElement);
            }
        }
        return mapping_array;
    } else {
        throw new Error('Argument passed to the function map is not of Array type')
    }
}

module.exports = map;