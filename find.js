

function find ( elements, callbackFunction ) {
    if(Array.isArray(elements)) {
        for( let index = 0; index < elements.length; index++) {
            if(callbackFunction(elements[index], index, elements)) {
                return elements[index];
            }
        }
    } else {
        throw new Error('Argument passed to the function find is not of Array type')
    }
}


module.exports = find;