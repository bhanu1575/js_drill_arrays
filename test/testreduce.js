let elements = require('../data.js');
const reduce = require('../reduce.js');

function calculateSum ( previousValue,currentValue ) {
    return previousValue + currentValue;
}

try {
    let sumOfAllElements = reduce(elements,calculateSum);
    console.log(sumOfAllElements);
} catch (error) {
    console.error(error.message);
}