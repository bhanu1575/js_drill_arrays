let elements = require('../data.js');
const filter = require('../filter.js');

function callbackFunction (element,index,array) {
    return (element % 2 != 0) 
}

try {
    let oddNumbers = filter(elements,callbackFunction);
    console.log(oddNumbers);
} catch (error) {
    console.error(error.message);
}

