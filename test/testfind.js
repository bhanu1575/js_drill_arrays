let elements = require('../data.js');
const find = require('../find.js');


function isEvenNumber( element, index, array) {
    return element % 2 == 0;
}

try {
    let firstEvenNumber = find(elements,isEvenNumber);
    console.log(firstEvenNumber);
} catch (error) {
    console.error(error.message);
}