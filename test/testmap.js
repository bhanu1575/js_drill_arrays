let items = require('../data.js');
let map = require('../map.js')


function squareOf ( element, index, array ) {
    return element**2;
}


try {
    let mapped_array = map(items,squareOf);
    console.log(mapped_array);
} catch (error) {
    console.error(error.message);
}