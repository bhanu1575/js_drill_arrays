const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'
const flatten = require('../flatten.js');
// const nestedArray = [1, [2], [3, [[4]]]];
// // const nestedArray = [1, [2, [3, 4]], 5];

try {
    let flattenArray = flatten(nestedArray);
    console.log(flattenArray);
} catch (error) {
    console.error(error.message);
}