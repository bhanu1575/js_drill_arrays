
function flatten(elements) {
    if (Array.isArray(elements)) {
        let flattenArray = [];

        for (element of elements) {
            if (Array.isArray(element)) {
                flattenArray.push(...flatten(element));
            } else {
                flattenArray.push(element);
            }
        }
        return flattenArray;
    } else {
        throw new Error('Argument passed to the function flatten is not of Array type');
    }
}


module.exports = flatten;