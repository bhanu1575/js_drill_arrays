

function reduce( elements, callbackFunction, startingValue) {
    if(Array.isArray(elements)) {
        startingValue = startingValue === undefined ? elements[0] : startingValue;
        for( let index = startingValue == elements[0] ? 1 : 0; index < elements.length; index++) {
            startingValue = callbackFunction(startingValue,elements[index]);
        }
        return startingValue;
    } else {
        throw new Error('Arguments passed to the function reduce is not of type array.')
    }
}

module.exports = reduce;